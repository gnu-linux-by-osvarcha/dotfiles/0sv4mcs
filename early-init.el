;;; early-init.el --- -*- lexical-binding: t -*-


;; DisableUnnecessaryInterface
(menu-bar-mode -1)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
;; -DisableUnnecessaryInterface

;; Disable package.el in favor of straight.el
(setq package-enable-at-startup nil)

;; No frame title
(setq frame-title-format nil)

;; Frame Maxime
;; (set-frame-parameter (selected-frame) 'fullscreen 'maximized)
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))
(message "early-init.el loaded successfully")
